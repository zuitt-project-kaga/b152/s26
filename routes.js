let http = require("http");
http
  .createServer((req, res) => {
    // console.log(req.url);
    if (req.url === "/") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("My Name Is Masa");
    }else if(req.url === "/login"){
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to the login page!");  
    }else{
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end("Not found Resource");  
    }
  })
  .listen(8000);

console.log("Server is running from localhost:8000");

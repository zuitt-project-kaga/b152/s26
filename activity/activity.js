let http = require("http");

http.createServer((req, res) => {
  let url = req.url;
  if (url === "/") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Welcome to B152 Booking System");
  } else if (url === "/courses") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Welcome to the Courses Page. View our Courses.");
  } else if (url === "/profile"){
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Welcome to the Profile. View your details.");
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end("Resource not found.");
  }
}).listen(8000);

console.log("Server is running from localhost:8000");